from loris.webapp import create_app

application = create_app(debug=False, config_file_path='/app/loris3.conf')
