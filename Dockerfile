FROM tiangolo/uwsgi-nginx:python3.8

LABEL MAINTAINER l.vanwissen@uva.nl

ENV TZ=Europe/Amsterdam
ENV DEBIAN_FRONTEND=noninteractive 
ENV NGINX_WORKER_PROCESSES 12
ENV UWSGI_INI /app/uwsgi.ini

COPY uwsgi.ini /app/uwsgi.ini
COPY loris.wsgi /app/loris.wsgi
COPY loris3.conf /app/loris3.conf
COPY nginx.conf /app/nginx.conf
COPY loris-nginx.conf /etc/nginx/conf.d/loris-nginx.conf

RUN useradd -d /var/www/loris3 -s /sbin/false loris

RUN apt update && apt install git ca-certificates build-essential gcc libjpeg-dev libfreetype6-dev zlib1g-dev \
	liblcms2-dev liblcms2-utils libtiff-dev libwebp-dev -y && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp

RUN git clone https://github.com/loris-imageserver/loris.git && mv ./loris/* /app/

RUN cp -r /app/loris/data/www/ /var/www/

WORKDIR /app

# Install dependencies and Loris for python3.8
RUN python3 setup.py install && pip3 install Pillow uwsgi pycparser


EXPOSE 80
