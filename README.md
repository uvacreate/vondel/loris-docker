# Loris Docker

## Loris

The default branch of Loris is git cloned from https://github.com/loris-imageserver/loris.

## Image

You can use the pre-built docker image in the container registry of this gitlab repository. A docker-compose example:

```yml
version: '3.8'
services:
  iiif:
    image: registry.gitlab.com/uvacreate/vondel/loris-docker:latest
    ports:
      - "8080:80"
    volumes:
      - ./data/image:/usr/local/share/images
    environment:
      - 'LORIS_PROXY_PATH=http://127.0.0.1:8080/iiif/2/'

```

### Build
Or build it yourself:

```
$ docker build -t loris .
```

## Run

```
$ docker run -it -v /path/to/local/images:/usr/local/share/images --rm loris
```

NB: Add ports when needed (`-p 8080:80`) and use the $LORIS_PROXY_PATH environment variable if running behind a proxy.

## Access

To make clear that images are served using the Image API 2.0 specification, the prefix of the request url is set to `iiif/2/`. Nginx redirects requests to this location.

The images in the mounted folder can be accessed through:

```
http://ipaddress/iiif/2/filename.jpg
```

Example:

```
http://127.0.0.1:8080/iiif/2/SK-A-4981.jpg/full/full/0/default.jpg
```
